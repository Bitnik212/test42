import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cars: []
  },
  mutations: {
    updatecars (state, cars) {
      Vue.set(state, 'cars', cars)
    }
  },
  actions: {
    async showcars (ctx) {
      let cars
      await axios.get('https://run.mocky.io/v3/dd45bd37-5ffc-4a14-b8df-d2bbb0a23d71').then(response => (cars = response))
      // console.log(cars.data)
      ctx.commit('updatecars', cars.data)
    }
  },
  getters: {
    allcars (state) {
      return state.cars
    }
  }
})
